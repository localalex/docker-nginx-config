# Docker Nginx Config
Current setup for deploying websites and services using Docker. It creates a Nginx Proxy that looks for other instances of docker containers and automatically creates proxies to those containers.

This repo serves as a quick example of what it can do. It is used to serve to websites, [eoinobrien.ie](https://eoinobrien.ie) and [blog.eoinobrien.ie](https://blog.eoinobrien.ie).

It also sets up a LetsEncrypt certificate that automatically renews for each domain.

## Structure
    +
    | - frontend                # Nginx Proxy
    |   | - docker-compose.yml
    | - eoinobrien.ie           # Static HTML website served by nginx
    |   | - docker-compose.yml
    | - blog.eoinobrien.ie      # Ghost blog 
    |   | - docker-compose.yml
        
## Thanks
Most of the rescources come from [https://darui.io/docker-nginx-letsencrypt/](https://darui.io/docker-nginx-letsencrypt/), I will be writting a blog post about my experiences soon.
